import path from 'path';
import fs from 'fs';
import dns from 'dns';
import {defineConfig} from 'vite';

dns.setDefaultResultOrder('verbatim');

// Make sure vite picks up all html files in root, needed for vite build
const allHtmlEntries = fs
    .readdirSync('.')
    .filter((file) => path.extname(file) === '.html')
    .reduce((acc, file) => {
        acc[path.basename(file, '.html')] = path.resolve(__dirname, file);

        return acc;
    }, {});

export default defineConfig({
    base: './',
    root: path.resolve(__dirname, '.'),
    build: {
        outDir: path.join(__dirname, 'public'),
        rollupOptions: {
            input: allHtmlEntries,
        },
    },
    server: {
        port: 3000,
    }
});
