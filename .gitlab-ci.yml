image: node:lts
include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml

stages:
  - init
  - build
  - test
  - security
  - package
  - iac_test
  - iac_plan
  - iac_destroy
  - iac_apply
  - assembly
  - deploy
  - release
  - rollback

# Needed CI variables:
#   GCP_PROJECT_ID
#   GCP_REGION
#   GCP_BUCKET_NAME_PREFIX

variables:
  ENV_FILE: ".env"
  PACKAGE_NAME: "webapp"
  DIST_DIR: "public/"
  DIST_FORMAT: "zip"

env-init:
  stage: init
  script:
    - echo "" > $ENV_FILE
    - echo "GCP_BUCKET_NAME_SUFFIX=$(date +%s)" >> $ENV_FILE
    - echo "ENV_FILE=$ENV_FILE"
    - echo ".env contents:"
    - cat $ENV_FILE
  artifacts:
    paths:
      - ${ENV_FILE}

npm-build:
  stage: build
  script:
    - npm install
    - npm run build
    - ls -ali public
  artifacts:
    paths:
      - ${DIST_DIR}

sast:
  stage: security

upload-package:
  stage: package
  needs:
    - job: npm-build
      artifacts: true
  script:
    - 'ls -ali'
    - 'ls -ali ${DIST_DIR}'
    - 'echo ">>> Compressing package..."'
    - 'zip ${DIST_DIR}.${DIST_FORMAT} ${DIST_DIR}' # FIXME Make dynamic based on DIST_FORMAT
    - 'echo "SUCCESS"'
    - 'echo ">>> Uploading package to project package registry..."'
#    - 'find ${DIST_DIR} -type -f -exec curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${DIST_DIR} "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGE_NAME}/0.0.1/${DIST_DIR}"'
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "${DIST_DIR}.${DIST_FORMAT}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGE_NAME}/0.0.1/${DIST_DIR}.${DIST_FORMAT}"'
    - 'echo "SUCCESS"'

terraform-validate:
  stage: iac_test
  image:
    name: hashicorp/terraform:latest
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  before_script:
    - cd iac
    - rm -rf .terraform
    - terraform --version
    - mkdir -p ./secure
    - echo $GCP_SA | base64 -d > ./secure/serviceaccount.json
    - terraform init
  script:
    - terraform validate

terraform-plan:
  when: manual
  stage: iac_plan
  image:
    name: hashicorp/terraform:latest
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  before_script:
    - cd iac
    - rm -rf .terraform
    - terraform --version
    - mkdir -p ./secure
    - echo $GCP_SA | base64 -d > ./secure/serviceaccount.json
    - terraform init
  script:
    - |
      terraform plan \
      -var="project_id=$GCP_PROJECT_ID" \
      -var="region=$GCP_REGION" \
      -var="webapp_bucket_name=$GCP_BUCKET_NAME_PREFIX-$GCP_BUCKET_NAME_SUFFIX-0x422e" \
      -out=./tf_plan.out
  artifacts:
    paths:
      - iac/tf_plan.out

terraform-destroy:
  when: manual
  stage: iac_destroy
  image:
    name: hashicorp/terraform:latest
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  before_script:
    - cd iac
    - rm -rf .terraform
    - terraform --version
    - mkdir -p ./secure
    - echo $GCP_SA | base64 -d > ./secure/serviceaccount.json
    - terraform init
  script:
    - |
      terraform plan \
      -var="project_id=$GCP_PROJECT_ID" \
      -var="region=$GCP_REGION" \
      -var="webapp_bucket_name=$GCP_BUCKET_NAME_PREFIX-$GCP_BUCKET_NAME_SUFFIX-0x422e" \
      -out=./tf_plan.out
    - terraform apply -destroy ./tf_plan.out
  artifacts:
    paths:
      - iac/tf.out
      - iac/tf.out.json

terraform-apply:
  stage: iac_apply
  image:
    name: hashicorp/terraform:latest
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  before_script:
    - cd iac
    - rm -rf .terraform
    - terraform --version
    - mkdir -p ./secure
    - echo $GCP_SA | base64 -d > ./secure/serviceaccount.json
    - terraform init
  script:
    - |
      terraform plan \
      -var="project_id=$GCP_PROJECT_ID" \
      -var="region=$GCP_REGION" \
      -var="webapp_bucket_name=$GCP_BUCKET_NAME_PREFIX-$GCP_BUCKET_NAME_SUFFIX-0x422e" \
      -out=./tf_plan.out
    - terraform apply ./tf_plan.out
    - terraform show -json
  artifacts:
    paths:
      - ${DIST_DIR}
      - iac/tf.out
      - iac/tf.out.json

download-package:
  stage: assembly
  needs:
    - job: upload-package
  script:
    - 'echo ">>> Downloading package from project package registry..."'
    - 'wget --header="JOB-TOKEN: $CI_JOB_TOKEN" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGE_NAME}/0.0.1/${DIST_DIR}.${DIST_FORMAT}'
    - 'echo "SUCCESS"'
    - 'ls -ali'
    - 'echo ">>> Decompressing package..."'
    - 'unzip "${DIST_DIR}.${DIST_FORMAT}"'
    - 'echo "SUCCESS"'
    - 'ls -ali'
    - 'ls -ali ${DIST_DIR}'
  artifacts:
    paths:
      - ${DIST_DIR}

gcp-deploy:
  stage: deploy
  needs:
    - job: terraform-apply
    - job: download-package
      artifacts: true
  image: google/cloud-sdk:latest
  environment:
    name: production
    url: https://storage.googleapis.com/mirodosapp--0x422e/app.html # FIXME Make dynamic
  script:
    - ls -ali
    - echo "Deploying web app..."
    - gsutil -m rsync -r -d ${DIST_DIR} gs://mirodosapp--0x422e
    - echo "SUCCESS"

pages:
  stage: release
  environment:
    name: development
    url: https://cziraky.gitlab.io/devops-origins
  script:
    - cp *.html /public
    - cp -R src/** /public
    - ls -ali /public
  artifacts:
    paths:
      # The folder that contains the files to be exposed at the Page URL
      - ${DIST_DIR}
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH