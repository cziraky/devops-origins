resource "google_storage_bucket" "webapp" {
  project       = var.project_id
  storage_class = "STANDARD"
  name          = var.webapp_bucket_name
  location      = var.region
  force_destroy = true

  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }

  cors {
    origin          = ["*"]
    method          = ["GET", "HEAD", "PUT", "POST"]
    response_header = ["*"]
    max_age_seconds = 3600
  }
}

resource "google_storage_default_object_access_control" "public_rule" {
  bucket = google_storage_bucket.webapp.name
  role   = "READER"
  entity = "allUsers"
}

resource "google_storage_bucket_iam_member" "member" {
  bucket = var.webapp_bucket_name
  role = "roles/storage.objectViewer"
  member = "allUsers"
}