terraform {
  backend "gcs" {
    credentials = "./secure/serviceaccount.json"
    bucket = "cziraky-io-tf-state"
  }
}