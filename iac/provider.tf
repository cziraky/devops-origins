variable "project_id" {
  type        = string
  description = "The ID of the GCP project"
}

variable "region" {
  type        = string
  description = "The desired GCP region"
}

variable "webapp_bucket_name" {
  type        = string
  description = "The name of the GCP Storage bucket for the web app"
}

provider "google" {
  credentials = file("./secure/serviceaccount.json")
  project     = var.project_id
  region      = var.region
}