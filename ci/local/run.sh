#!/usr/bin/env bash
for job in $(yq e 'keys' .gitlab-ci.yml | sed 's/^- //g ; /^stages/d ; /^image/d ; /^include/d ; /^variables/d ; /^sast/d ; /#/d ; /^\s*$/d'); do
    echo ">>> Executing $job..."
    gitlab-runner exec shell $job
done