var md = md || window.markdownit();
let urlSearchParams = new URLSearchParams(window.location.search);
let topic = atob(urlSearchParams.get('topic'));
let styleData = JSON.parse(atob(urlSearchParams.get('style')));

function init() {
    loadContent('general', topic);
    invokeDetailsGPT('' +
        'Tell me about ' + topic + ' regarding DevOps. ' +
        'Then list the top 10 state of the art methodologies related to ' + topic + ' regarding DevOps. ' +
        'Please make sure that the sections are well separated with new lines ' +
        'and easily readable in your response. ' +
        'The response must be a well-formatted MARKDOWN. ' +
        'List items should be paragraph-like MARKDOWN items with a level 3 header and a normal respective content. ' +
        'Always add one extra newline after each paragraph item in your MARKDOWN response. ' +
        'Maximum list level must be 1 in your MARKDOWN response. ' +
        'Each formula should be an inline-code and started in a new line in your MARKDOWN response.', 'generalDetails');

    loadContent('tools', 'Tools');
    invokeDetailsGPT('' +
        'List the top 10 state of the art tools and frameworks nowadays used for ' + topic + ' regarding DevOps. ' +
        'Please make sure that the sections are well separated with new lines ' +
        'and easily readable in your response. ' +
        'The response must be a well-formatted MARKDOWN. ' +
        'List items should be paragraph-like MARKDOWN items with a level 3 header and a normal respective content. ' +
        'Always add one extra newline after each paragraph item in your MARKDOWN response. ' +
        'Maximum list level must be 1 in your MARKDOWN response. ' +
        'Each formula should be started in a new line in your MARKDOWN response.', 'toolsDetails');
    loadContent('metrics', 'Metrics');
    invokeDetailsGPT('' +
        'List common metrics with formulae as code block for ' + topic + '. ' +
        'Please make sure that the sections are well separated with new lines ' +
        'and easily readable in your response. ' +
        'The response must be a well-formatted MARKDOWN. ' +
        'List items should be paragraph-like MARKDOWN items with a level 3 header and a normal respective content. ' +
        'Always add one extra newline after each paragraph item in your MARKDOWN response. ' +
        'Maximum list level must be 1 in your MARKDOWN response. ' +
        'Each formula should be started in a new line in your MARKDOWN response.', 'metricsDetails');
    loadContent('codeSnippets', 'Code Snippets');
    invokeDetailsGPT('' +
        'List code snippets for ' + topic + '  regarding DevOps and mention the source presented as FQDN. ' +
        'Please make sure that the sections are well separated with new lines ' +
        'and easily readable in your response. ' +
        'The response must be a well-formatted MARKDOWN. ' +
        'List items should be paragraph-like MARKDOWN items with a level 3 header and a normal respective content. ' +
        'Always add one extra newline after each paragraph item in your MARKDOWN response. ' +
        'Maximum list level must be 1 in your MARKDOWN response. ' +
        'Each formula should be started in a new line in your MARKDOWN response.', 'codeSnippetsDetails');
}

function loadContent(elementId, title) {
    let node = document.createElement('div');
    let nodeStyle = `
            margin: 0;            
            border: ${styleData.border};
            border-top-left-radius: 10px;
            border-bottom-right-radius: 10px;
        `;
    let taskTitleNode = document.createElement('div');
    let taskDescNode = document.createElement('div');

    taskTitleNode.innerHTML = title.replace(/<\/?[^>]+(>|$)/g, "");
    let taskTitleStyle = `            
            border-top-left-radius: 5px;
            border-bottom-right-radius: 0px;
            padding-left: 0;
            margin-right: 0;
            border: 0;
            display: inline-felx;
            height: 26px;
            width: 100%;
            background-color: ${styleData.backgroundColor};
            color: ${styleData.color};
            font-size: 16px;
            font-weight: bold;
            align-items: center;
            justify-content: left;
        `;

    taskDescNode.innerHTML = topic.replace(/<\/?[^>]+(>|$)/g, "");
    node.innerHTML = `
            <table class='details' style='${nodeStyle}'>
                <tbody>
                    <tr>
                        <td class='taskTitle' style='${taskTitleStyle}'>&nbsp;${taskTitleNode.innerText}</td>
                    </tr>
                    <tr>
                        <td>
                            <div class='overflow'>
                                <div id='${elementId}Details' class='taskDescription'></div>
                            </div>                                                        
                        </td>
                    </tr>
                </tbody>
            </table>
        `;
    node.style.height = '100%';
    document.getElementById(elementId).appendChild(node);
}

init();