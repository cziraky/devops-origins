var md = md || window.markdownit();

function createQANode(idSuffix, question, answer, avatar) {
    let qNode = document.createElement('p');
    let preNode = document.createElement('pre');
    let aNode = document.createElement('p');
    let qaNode = document.createElement('p');
    qNode.setAttribute('class', 'question');
    aNode.setAttribute('class', 'answer');
    qaNode.setAttribute('class', 'draggable miro-draggable');
    qNode.innerHTML = '<table><tr><td>' + question + '</td><td class="avatar">👤[YOU]</td></tr></table>';

    preNode.innerHTML = answer;
    const noLangCodeBlockRegExp = /```\n([\s\S]*?)\n```/g; // TODO: Implement...
    const codeBlockRegExp = /`{3}([\S]+)?\n([\s\S]+)\n`{3}/g;
    const languageRegExp = /`{3}([\S]+)?\n/g;
    let codeBlock = preNode.innerHTML.match(codeBlockRegExp);
    const hasCodeBlock = (codeBlock && (codeBlock[0] !== ''));
    let finalResponse;
    if (hasCodeBlock) {
        let language = codeBlock[0].toString().match(languageRegExp)[0].replace('```', '');
        let codeBlockNode = `
            <div id="gptCodeBlockHeader${idSuffix}" class="codeBlockHeader">${language}</div>
            <textarea id="gptCodeBlock${idSuffix}">${codeBlock[0].replace(languageRegExp, '').replaceAll('```', '')}</textarea>
            <div id="gptCodeBlockFooter${idSuffix}" class="codeBlockFooter"></div>`;

        finalResponse = preNode.innerHTML.replace(codeBlockRegExp, '```C_BLOCK```');
        finalResponse = finalResponse.replaceAll('\n', '<br/>');
        finalResponse = finalResponse.replace('```C_BLOCK```', codeBlockNode);
    } else {
        finalResponse = preNode.innerHTML.replaceAll('\n', '<br/>');
    }
    aNode.innerHTML = '<span class="avatar">' + avatar + '</span>&nbsp;<p id="gptResponse" class="gptResponse">' + md.render(answer) + '</p>';
    aNode.style.maxWidth = `${DOS_RESPONSE_ELEMENT_MAX_WIDTH}px`;

    qaNode.appendChild(qNode);
    qaNode.appendChild(aNode);
    qaNode.setAttribute('data-code', (hasCodeBlock === true ? 'true' : 'false'));
    return qaNode;
}