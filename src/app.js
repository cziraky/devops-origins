import {OPENAI_TOKEN} from "./config";

const DOS_RESPONSE_ELEMENT_MAX_WIDTH = 352;
var md = md || window.markdownit();

let commandHistory = {
    dos: {
        cursor: 0,
        commands: []
    },
    chat: {
        cursor: 0,
        commands: []
    }
};

let journey = {
    step: -1
};

const questionForm = document.getElementById('questionForm');
const prompt = document.getElementById('question');
const searchForm = document.getElementById('searchForm');
const search = document.getElementById('search');
const dosSteps = document.getElementById('dosNodeList');
const conversation = document.getElementById('conversation');

async function init() {
    await initDOS('You find below the steps defined in this Miro board. Click on the "more" button to get additional information.');
    document.onkeyup = (key) => {
        // key.preventDefault(); // Here it block's every keys...

        if (![search, prompt].includes(key.target)) {
            // Context: document.
            switch (key.code) {
                case 'ArrowLeft':
                    key.preventDefault(); // FIXME: Not really working...
                    if (journey.step > 0) {
                        prev()
                    }
                    break;
                case 'ArrowRight':
                    key.preventDefault(); // FIXME: Not really working...
                    next();
                    break;
                default: return;
            }
        } else {
            //Context: DOS and GPT input fields.
            let cur = 0
            switch (key.code) {
                case 'ArrowUp':
                    key.preventDefault(); // FIXME: Not really working...
                    cur = Math.max(0, --commandHistory.chat.cursor)
                    if (key.target === prompt) prompt.value = commandHistory.chat.commands[cur];
                    else if (key.target === search) prompt.value = commandHistory.dos.commands[cur];
                    break;
                case 'ArrowDown':
                    key.preventDefault(); // FIXME: Not really working...
                    cur = Math.min(commandHistory.chat.commands.length - 1, ++commandHistory.chat.cursor)
                    if (key.target === prompt) prompt.value = commandHistory.chat.commands[cur];
                    else if (key.target === search) prompt.value = commandHistory.dos.commands[cur];
                    break;
                default: return;
            }
        }
    }
}

questionForm.addEventListener('submit', async (event) => {
    event.preventDefault();

    if (prompt.value) {
        commandHistory.chat.commands.push(prompt.value);
        commandHistory.chat.cursor = Math.max(0, commandHistory.chat.commands.length - 1);
    }

    // Handle prompt types
    if (prompt.value.startsWith('::')) {
        await parseSystemPrompt(prompt.value);
    } else if (prompt.value.startsWith(':')) {
        await parseDOSPrompt(prompt.value);
    } else {
        await askGPT(prompt.value);
    }
});

searchForm.addEventListener('submit', async (event) => {
    event.preventDefault();

    if (search.value) {
        commandHistory.dos.commands.push(search.value);
        commandHistory.dos.cursor = Math.max(0, commandHistory.dos.commands.length - 1);
    }

    // Handle prompt types
    if (search.value.startsWith('::')) {
        await parseSystemPrompt(search.value);
    } else if (search.value.startsWith(':')) {
        await parseDOSPrompt(search.value);
    }
});

async function prev() {
    const node = document.getElementById('task' + journey.step);
    const controlNode = document.getElementById('taskControl' + journey.step);
    await prevControlEvent(null, controlNode, node);
}

async function next() {
    const node = document.getElementById('task' + journey.step);
    const controlNode = document.getElementById('taskControl' + journey.step);
    await nextControlEvent(null, controlNode, node);
}

async function prevControlEvent(event, controlNode, node) {
    if (event) {
        event.preventDefault();
        // BEGIN Turn off intersecting event bubbling and capturing
        if (!event) var event = window.event;
        event.cancelBubble = true;
        if (event.stopPropagation) event.stopPropagation();
        // END Turn off intersecting event bubbling and capturing
    }

    controlNode.dispatchEvent(new Event('click', {bubbles: true}));
    const index = parseInt(node.id.match(/\d+/)[0]);
    const prevTask = document.getElementById(`task${index - 1}`);
    const prevControl = document.getElementById(`taskControl${index - 1}`);
    // prevTask.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
    dosSteps.scrollTop = prevTask.offsetTop - 100; // FIXME: Calculate that value...
    prevControl.dispatchEvent(new Event('click', {bubbles: true}));
    journey.step = index - 1;
}

async function nextControlEvent(event, controlNode, node) {
    if (event) {
        event.preventDefault();
        // BEGIN Turn off intersecting event bubbling and capturing
        if (!event) var event = window.event;
        event.cancelBubble = true;
        if (event.stopPropagation) event.stopPropagation();
        // END Turn off intersecting event bubbling and capturing
    }

    controlNode.dispatchEvent(new Event('click', {bubbles: false}));
    const index = parseInt(node.id.match(/\d+/)[0]);
    const nextTask = document.getElementById(`task${index + 1}`);
    const nextControl = document.getElementById(`taskControl${index + 1}`);
    // nextTask.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
    dosSteps.scrollTop = nextTask.offsetTop - 100; // FIXME: Calculate that value...
    nextControl.dispatchEvent(new Event('click', {bubbles: true}));
    journey.step = index + 1;
}

async function openModalEvent(event, url, fullScreen = false) {
    event.preventDefault();
    await miro.board.ui.openModal({
        url: url,
        width: 1820,
        height: 1024,
        fullscreen: fullScreen,
    });
}

async function moreLessControlEvent(event, controlNode, node, detailsNode, descNode, controlPanelNode) {
    event.preventDefault();
    if (controlNode.innerText === 'more') {
        if (!controlNode.hasAttribute("gpt-loaded")) {
            await invokeGPT('' +
                'I will first describe how to generate your response as a list of instructions labeled as INSTRUCTIONS, then at the end of this request I will describe my request labeled as REQUEST' +
                '' +
                'INSTRUCTIONS: ' +
                '- The response must be a well-formatted MARKDOWN. ' +
                '- List items should be paragraph-like MARKDOWN items with a level 3 header and a normal respective content. ' +
                '- Always add one extra newline after each paragraph item in your MARKDOWN response. ' +
                '- Maximum list level must be 1 in your MARKDOWN response. ' +
                '- Each formula should be started in a new line in your MARKDOWN response.' +
                '- Please make sure that the sections (General, Methodologies, Tools and Frameworks) are well separated with new lines and easily readable in your response.' +
                '- Limit your response to 150 words. ' +
                '' +
                'REQUEST:' +
                '- **General** Tell me about ' + node.innerText + '. ' +
                '**Methodologies** Then list the top 3 state of the art methodologies. ' +
                '- **Tools and Frameworks** Then list the top 3 state of the art tools and frameworks nowadays used for ' + node.innerText + '. ',
                // '- **GitLab Ultimate** Then list the built-in tools GitLab Ultimate can offer for ' + node.innerText + '. ' +
                // '- **Metrics** Finally, list all state of the art metrics with formulae as inline-code block for ' + node.innerText + '. ' +
                controlNode, detailsNode, descNode);
        } else {
            detailsNode.style.display = 'block';
        }
        controlNode.innerText = 'less';
        controlPanelNode.style.borderBottomRightRadius = '0';
        controlNode.setAttribute("gpt-loaded", "true");
    } else {
        detailsNode.style.display = 'none';
        controlNode.innerText = 'more';
        controlPanelNode.style.borderBottomRightRadius = '5px';
    }
}

function populateNested(question, htmlNode, avatar) {
    dosSteps.insertBefore(createDOSNode(question, htmlNode, avatar), dosSteps.firstChild);

    let nodes = document.getElementsByClassName('taskTitle');
    Array.prototype.forEach.call(nodes, function (node) {

        const wrapperNode = document.getElementById(node.id.replace('taskTitle', 'task'));
        const titleNode = document.getElementById(node.id.replace('taskTitle', 'taskTitle'));
        const descNode = document.getElementById(node.id.replace('taskTitle', 'taskDescription'));
        const controlPanelNode = document.getElementById(node.id.replace('taskTitle', 'taskControlPanel'));
        const controlNode = document.getElementById(node.id.replace('taskTitle', 'taskControl'));
        const controlPrev = document.getElementById(node.id.replace('taskTitle', 'taskControlPrev'));
        const controlNext = document.getElementById(node.id.replace('taskTitle', 'taskControlNext'));
        const controlDetails = document.getElementById(node.id.replace('taskTitle', 'taskControlDetails'));
        const controlTechWheel = document.getElementById(node.id.replace('taskTitle', 'taskControlTechWheel'));
        const detailsNode = document.getElementById(node.id.replace('taskTitle', 'taskDetails'));

        controlNode.addEventListener('click', async (event) => {
            await moreLessControlEvent(event, controlNode, node, detailsNode, descNode, controlPanelNode);
        });

        controlDetails.addEventListener('click', async (event) => {
            const style = {
                border: window.getComputedStyle(wrapperNode).getPropertyValue('border'),
                backgroundColor: titleNode.style.backgroundColor,
                color: titleNode.style.color
            };
            await openModalEvent(event, `details.html?topic=${btoa(titleNode.innerText)}&style=${btoa(JSON.stringify(style))}`);
        });

        controlTechWheel.addEventListener('click', async (event) => {
            await openModalEvent(event, `tech-wheel.html?topic=${btoa(titleNode.innerText)}`);
        });

        controlNext.addEventListener('click', async (event) => {
            await nextControlEvent(event, controlNode, node);
        });

        controlPrev.addEventListener('click', async (event) => {
            await prevControlEvent(event, controlNode, node);
        });
    });
    prompt.value = '';
}

function createDOSNode(question, htmlNode, avatar) {
    let qNode = document.createElement('p');
    let qaNode = document.createElement('div');
    qNode.setAttribute('class', 'question');
    qNode.innerHTML = '<span class="avatar">' + avatar + '</span>&nbsp;<span>' + question + '</span>';
    qaNode.appendChild(qNode);
    qaNode.appendChild(htmlNode);
    return qaNode;
}

async function parseSystemPrompt(prompt) {
    let response = '';
    const command = prompt.split('::')[1].trim();
    if (command.startsWith('openai set apikey')) {
        const commandTokens = command.split(' ');
        openai.apiKey = commandTokens[commandTokens.length - 1];
        response += '\n';
        response += 'OpenAI API key has been set to: ' + openai.apiKey;
    } else if (command.startsWith('openai set model')) {
        const commandTokens = command.split(' ');
        openai.model = commandTokens[commandTokens.length - 1];
        response += '\n';
        response += 'OpenAI model has been set to: ' + openai.model;
    } else if (command.startsWith('dos init')) {
        response += '\n';
        response += 'Initializing your DevOps Origins journey in Miro...';
        await initDOS(prompt);
    } else if (command.startsWith('resize')) {
        document.querySelector('[data-testid="toolbar-library-container"]').style.width = '600px';
    } else {
        response = 'command not found';
    }
    return response;
}

function parseDOSPrompt(prompt) {
    const command = prompt.split(':')[1].trim();
    if (command.startsWith('help')) {
        populate(prompt, 'help info...', '👾[DOS]')
    } else if (command.startsWith('reset')) {
        populate(prompt, 'reset...', '👾[DOS]')
    }
}

async function initDOS(input) {
    const connectors = await miro.board.get({
        type: ['connector']
    });

    const shapes = await miro.board.get({
        type: ['shape']
    });

    let steps = [];
    shapes.forEach(startShape => {
        connectors.forEach(connector => {
            if (startShape.shape === 'circle' && connector.start !== undefined && connector.start.item === startShape.id) {
                shapes.forEach(endShape => {
                    if (endShape.shape === 'rectangle' && connector.end !== undefined && connector.end.item === endShape.id) {
                        steps.push({
                            step: startShape,
                            title: endShape
                        });
                    }
                });
            }
        });
    });

    steps.sort(function (a, b) {
        let aKey = parseInt(a.step.content.replace(/<\/?[^>]+(>|$)/g, ""));
        let bKey = parseInt(b.step.content.replace(/<\/?[^>]+(>|$)/g, ""));
        if (aKey > bKey) return 1;
        if (aKey < bKey) return -1;
        return 0;
    });

    let tasksListNode = document.createElement('div');
    let taskIdx = 0;
    steps.forEach(step => async function () {
        let taskNode = document.createElement('div');
        let taskNodeStyle = `
            padding 0;
            margin: 0;            
            border: 2px ${step.step.style.borderStyle === 'normal' ? 'solid' : step.step.style.borderStyle} ${step.step.style.borderColor};
            border-top-left-radius: 10px;
            border-bottom-right-radius: 10px;
        `;
        let taskStepNode = document.createElement('div');
        let taskTitleNode = document.createElement('div');
        let taskDescNode = document.createElement('div');

        taskStepNode.innerText = step.step.content.replace(/<\/?[^>]+(>|$)/g, "");
        let taskStepStyle = `
            padding-left: 1px;
            border-top-left-radius: 5px;
            display: inline-felx;
            width: 40px;
            height: 26px;
            background-color: ${step.step.style.fillColor};
            color: ${step.step.style.color};
            font-size: 12px;
            font-weight: bold;
            align-items: center;
            justify-content: left;
            cursor: pointer;
        `;

        taskTitleNode.innerHTML = step.title.content.replace(/<\/?[^>]+(>|$)/g, "");
        let taskTitleStyle = `            
            padding-left: 3px;
            margin: 0;
            border: 0;
            display: inline-block;
            width: 245px;
            height: 26px;
            max-width: 245px;
            max-height: 26px;
            background-color: ${step.step.style.fillColor};
            color: ${step.step.style.color};
            font-size: 14px;
            font-weight: bold;
            align-items: center;
            justify-content: left;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
        `;

        taskDescNode.innerHTML = step.title.content.replace(/<\/?[^>]+(>|$)/g, "");
        let taskDescStyle = `            
            padding-left: 3px;
            margin: 0;
            border: 0;
            display: inline-felx;
            width: 319px;
            background-color: ${step.step.style.fillColor};
            color: ${step.step.style.color};
            font-size: 16px;
            font-weight: bold;
            align-items: center;
            justify-content: left;
        `;

        let taskControlStyle = `       
            border-bottom-right-radius: 5px;
            background-color: ${step.step.style.fillColor};
            background-blend-mode: difference;
            cursor: pointer;
        `;

        let taskWrapperId = 'taskWrapper' + taskIdx;
        let taskId = 'task' + taskIdx;
        let taskTitleId = 'taskTitle' + taskIdx;
        let taskControlPanelId = 'taskControlPanel' + taskIdx;
        let taskControlId = 'taskControl' + taskIdx;
        let taskControlPrevId = 'taskControlPrev' + taskIdx;
        let taskControlNextId = 'taskControlNext' + taskIdx;
        let taskControlDetailsId = 'taskControlDetails' + taskIdx;
        let taskControlTechWheelId = 'taskControlTechWheel' + taskIdx;
        let taskDescriptionId = 'taskDescription' + taskIdx;
        let taskDetailsId = 'taskDetails' + taskIdx;

        taskNode.innerHTML = `
            <table id='${taskId}' class='dos' style='${taskNodeStyle}'>
                <tbody>
                    <tr>
                        <td style='${taskStepStyle}'>&nbsp;${taskStepNode.innerText}</td>
                        <td id='${taskTitleId}' class='taskTitle' style='${taskTitleStyle}'>${taskTitleNode.innerText}</td>
                        <td id='${taskControlPanelId}' class='taskControlPanel' style='${taskControlStyle}'><button id='${taskControlId}' class='taskControl'>more</button></td>
                    </tr>
                    <tr id='${taskDetailsId}' class="taskDetails">
                        <td class='taskDescription'>
                            <p id='${taskDescriptionId}' class="gptResponse"></p>                            
                        </td>
                        <td class='taskInnerControlPanel' style='${taskControlStyle}'>
                            <button id='${taskControlPrevId}' class='taskControl innerControl' style='visibility: ${taskIdx === 0 ? "hidden" : "visible"}'>prev</button>
                            <button id='${taskControlNextId}' class='taskControl innerControl'>next</button>
                            <button id='${taskControlDetailsId}' class='taskControl innerControl'>details</button>
                            <button id='${taskControlTechWheelId}' class='taskControl innerControl'>wheel</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        `;
        taskNode.id = taskWrapperId;
        taskNode.setAttribute('class', 'task');
        taskNode.addEventListener('click', async (event) => {
                event.preventDefault();
                journey.step = parseInt(taskNode.id.match(/\d+/)[0]);
                miro.board.viewport.zoomTo(step.title);
            }
        );
        tasksListNode.appendChild(taskNode);
        taskIdx++;
    }());
    populateNested(input, tasksListNode, '👾[DOS]');
}

init().then(r => {
    console.warn("DOS Miro app initialized...");
});