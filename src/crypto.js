/**
 * Compresses a string using LZW compression algorithm.
 * @param {string} text - The input string to be compressed.
 * @returns {string} - The compressed string.
 */
function compressString(text) {
    var dictionary = {};
    var compressed = "";
    var nextCode = 256;

    for (var i = 0; i < 256; i++) {
        dictionary[String.fromCharCode(i)] = i;
    }

    var currentChar = "";
    for (var j = 0, len = text.length; j < len; j++) {
        var newChar = currentChar + text[j];
        if (dictionary.hasOwnProperty(newChar)) {
            currentChar = newChar;
        } else {
            compressed += String.fromCharCode(dictionary[currentChar]);
            dictionary[newChar] = nextCode++;
            currentChar = text[j];
        }
    }

    if (currentChar !== "") {
        compressed += String.fromCharCode(dictionary[currentChar]);
    }

    return compressed;
}

/**
 * Decompresses a string using LZW compression algorithm.
 * @param {string} compressedText - The compressed string to be decompressed.
 * @returns {string} - The decompressed string.
 */
function decompressString(compressedText) {
    var dictionary = {};
    var decompressed = "";
    var nextCode = 256;

    for (var i = 0; i < 256; i++) {
        dictionary[i] = String.fromCharCode(i);
    }

    var currentCode = compressedText.charCodeAt(0);
    var previousString = dictionary[currentCode];
    var newString = previousString;

    decompressed += previousString;

    for (var j = 1, len = compressedText.length; j < len; j++) {
        currentCode = compressedText.charCodeAt(j);

        if (dictionary[currentCode]) {
            newString = dictionary[currentCode];
        } else {
            newString = previousString + previousString[0];
        }

        decompressed += newString;

        dictionary[nextCode++] = previousString + newString[0];

        previousString = newString;
    }

    return decompressed;
}