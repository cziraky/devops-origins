// I need a div in a table td that has an automatic y-overflow. The table and td height should always be 100% of the window/document height.

const DOS_RESPONSE_ELEMENT_MAX_WIDTH = 352;
var md = md || window.markdownit();

let openai = {
    model: 'gpt-4-1106-preview',
    param: {

    },
    apiKey: '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',
    system: {
        chat: [
            "You are a helpful scientific assistant."
        ],
        dos: [
            "You are a DevOps Engineer.",
            "The conversation context is Software Development Lifecycle (SDLC) and DevOps.",
            "The aim of the conversation is to learn best practices and new knowledge.",
            "Every response must be a well-formatted MARKDOWN. That is important.",
            "Limit your response to 150 words."
        ],
        memory: {
            length: 3,
            maxLength: 5,
            entries: []
        }
    }
};

async function askGPT(question) {
    const url = "https://api.openai.com/v1/chat/completions";
    const bearer = 'Bearer ' + openai.apiKey;

    let messages = [];
    openai.system.chat.forEach(message => {
        messages.push({role: "system", content: message});
    });

    const memoryLength = openai.system.memory.entries.length;
    let lastMemory = memoryLength > 0 ? openai.system.memory.entries[memoryLength - 1] : '';
    messages.push({role: "user", content: (question + ' ' + lastMemory + ' The response has to be a well-formatted MARKDOWN.').trim()});
    console.log(messages);

    fetch(url, {
        method: 'POST',
        headers: {
            'Authorization': bearer,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({model: openai.model, messages: messages})
    }).then(response => {
        return response.json()
    }).then(async data => {
        const content = data['choices'][0].message.content;
        openai.system.memory.entries.push(content);
        populate(question, content, '🤖[AI]');
    }).catch(error => {
        console.error(error);
        populate(prompt.value, error, '❌[ERR]');
    });
}

async function generateTechWheelData(question, runtime, define, inspector) {
    const url = "https://api.openai.com/v1/chat/completions";
    const bearer = 'Bearer ' + openai.apiKey;
    let messages = [];
    messages.push({role: "user", content: (question).trim()});
    console.log(messages);
    fetch(url, {
        method: 'POST',
        headers: {
            'Authorization': bearer,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({model: openai.model, messages: messages})
    }).then(response => {
        return response.json()
    }).then(async data => {
        wheelData = JSON.parse(data['choices'][0].message.content);
        console.log(wheelData);
        const main = runtime.module(define, inspector.into(document.getElementById('wheel')));
    }).catch(error => {
        console.error(error);
    });
}

async function invokeGPT(question, controlNode, detailsNode, targetNode) {
    controlNode.style.display = 'none';
    detailsNode.style.display = 'block';
    // targetNode.innerText = 'Asking a robot, please wait...';
    const timer = startTimer(targetNode)
    // stopper(timer, targetNode);
    const url = "https://api.openai.com/v1/chat/completions";
    const bearer = 'Bearer ' + openai.apiKey;

    let messages = [];
    openai.system.dos.forEach(message => {
        messages.push({role: "system", content: message});
    });
    messages.push( {role: "user", content: question});

    fetch(url, {
        method: 'POST',
        headers: {
            'Authorization': bearer,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({model: openai.model, messages: messages})
    }).then(response => {
        return response.json()
    }).then(async data => {
        const responseTime = timer.getElapsedTime();
        timer.stop(); // Stop the counter thread when you're done with it
        targetNode.innerHTML = `<p style="font-size: 14px; background-color: rgba(255, 255, 255, 0.1); padding: 5px; width: ${DOS_RESPONSE_ELEMENT_MAX_WIDTH - 26}px; display: inline-flex;">// ${openai.model} | ${responseTime.seconds}.${responseTime.milliseconds}s</p>` + md.render(data['choices'][0].message.content);
        hljs.highlightAll();
        targetNode.style.maxWidth = `${DOS_RESPONSE_ELEMENT_MAX_WIDTH}px`;
        controlNode.style.display = 'inline-flex';
    }).catch(error => {
        controlNode.style.display = 'inline-flex';
        console.error(error);
        return error;
    });
}

async function invokeDetailsGPT(question, targetNodeId) {
    const targetNode = document.getElementById(targetNodeId);
    targetNode.innerText = 'Asking a robot, please wait...';
    const url = "https://api.openai.com/v1/chat/completions";
    const bearer = 'Bearer ' + openai.apiKey;

    let messages = [];
    openai.system.dos.forEach(message => {
        messages.push({role: "system", content: message});
    });
    messages.push( {role: "user", content: question});

    fetch(url, {
        method: 'POST',
        headers: {
            'Authorization': bearer,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({model: openai.model, messages: messages})
    }).then(response => {
        return response.json()
    }).then(async data => {
        targetNode.innerHTML = md.render(data['choices'][0].message.content);
        hljs.highlightAll();
    }).catch(error => {
        console.error(error);
        return error;
    });
}

function populate(question, content, avatar) {
    const nodeIdSuffix = new Date().getUTCMilliseconds();
    const node = createQANode(nodeIdSuffix, question, content, avatar);
    conversation.insertBefore(node, conversation.firstChild);
    hljs.highlightAll();
    prompt.value = '';
}

function startTimer(targetNode) {
    let startTime = Date.now();

    // The interval ID for clearing it later if needed
    let intervalId = setInterval(() => {
        let elapsedTime = Date.now() - startTime;
        targetNode.innerHTML = `<p style="font-size: 14px; background-color: rgba(255, 255, 255, 0.1); padding: 5px; width: ${DOS_RESPONSE_ELEMENT_MAX_WIDTH - 26}px; display: inline-flex;">// ${openai.model} | ${Math.floor(elapsedTime / 1000)}.${elapsedTime % 1000}s<br/>// Waiting for AI response...</p>`;
    }, 100); // Update interval; you can change this value as needed

    return {
        getElapsedTime: function() {
            let elapsedTime = Date.now() - startTime;
            return {
                seconds: Math.floor(elapsedTime / 1000),
                milliseconds: elapsedTime % 1000
            };
        },
        stop: function() {
            clearInterval(intervalId);
        }
    };
}

async function stopper(timer, targetNode) {
// Example of getting the elapsed time after 3 seconds
    setTimeout(() => {
        targetNode.HTML = `<p style="font-size: 14px; background-color: rgba(255, 255, 255, 0.3); padding: 5px; width: ${DOS_RESPONSE_ELEMENT_MAX_WIDTH - 26}px; display: inline-flex;">// ${openai.model} | Maybe it's sleeping... :/<br/>// Waiting for AI response...</p>`;
        timer.stop();
    }, 30000);
}